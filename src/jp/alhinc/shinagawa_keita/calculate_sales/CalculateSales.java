package jp.alhinc.shinagawa_keita.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class CalculateSales {
	public static void main(String[] args) {

		if (args.length != 1) {
			//コマンドライン引数に誤りがあればエラーメッセージを表示して処理を終了
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		BufferedReader br = null;

		//支店コードと、コードに対応した支店名を格納するMapを宣言
		HashMap<String, String> branchMap = new HashMap<String, String>();

		//支店コードと、コードに対応した売上金額の初期値(0)を格納するMapを宣言
		HashMap<String, Long> salesMap = new HashMap<String, Long>();

		try {
			//ファイルパスと名前を指定してFileオブジェクトの生成
			File file = new File(args[0], "branch.lst");

			if (!file.exists()) {
				System.out.println("支店定義ファイルが存在しません");
				return;
			}

			//FileReaderオブジェクトを生成
			FileReader fr = new FileReader(file);
			//BufferedReaderオブジェクトの生成
			br = new BufferedReader(fr);

			String line;

			//ファイル内部の読込
			while ((line = br.readLine()) != null) {

				//支店コードと支店名を分割
				String[] branchInfo = line.split(",", 0);

				//支店定義ファイルのフォーマットに誤りがある場合はエラーメッセージを表示して処理を終了
				if ((!branchInfo[0].matches("[0-9]{3}") || (branchInfo.length != 2))) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}

				//支店コードと支店名をbranchMapに格納
				branchMap.put(branchInfo[0], branchInfo[1]);
				//支店コードと売上金額の初期値(0)をsalesMapに格納
				salesMap.put(branchInfo[0], (long) 0);
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if (br != null) {
				try {
					//BufferedReaderの終了
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}

		File file = new File(args[0]);

		//ディレクトリ内のファイル一覧を配列に格納
		File[] fileList = file.listFiles();

		//売上ファイルを格納するArrayListを宣言
		ArrayList<File> salesFiles = new ArrayList<File>();

		for (int i = 0; i < fileList.length; i++) {
			//ディレクトリ内のファイルを上から順番に参照し、条件と一致するか判定
			if (fileList[i].getName().matches("[0-9]{8}.rcd") && fileList[i].isFile()) {
				//条件と一致したファイルをArrayListに格納
				salesFiles.add(fileList[i]);
			}
		}

		//売上ファイル名が連番になっているかを確認
		for (int i = 0; i < salesFiles.size() - 1; i++) {

			int fileNum0 = Integer.parseInt(salesFiles.get(i).getName().substring(0, 8));

			int fileNum1 = Integer.parseInt(salesFiles.get(i + 1).getName().substring(0, 8));

			//売上ファイル名が連番になっていなければエラーメッセージを表示して処理を終了
			if (fileNum1 - fileNum0 != 1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		for (int i = 0; i < salesFiles.size(); i++) {
			try {
				//ファイル読込用の各オブジェクトを生成
				FileReader fr = new FileReader(salesFiles.get(i));
				br = new BufferedReader(fr);

				//売上ファイルの支店コードと売上金額を格納するリストを宣言
				ArrayList<String> salesList = new ArrayList<String>();

				String line;

				//売上ファイルの支店コードと売上金額を読み込んでsalesList内に格納
				while ((line = br.readLine()) != null) {
					salesList.add(line);
				}

				//売上ファイル内の行数が不正な場合はエラーメッセージを表示して処理を終了
				if (salesList.size() != 2) {
					System.out.println(fileList[i].getName() + "のフォーマットが不正です");
					return;
				}
				//売上ファイルのフォーマットが不正な場合はエラーメッセージを表示して処理を終了
				else if (!salesList.get(1).matches("[0-9]+")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				//支店コードと売上金額を変数に格納
				String branchCode = salesList.get(0);
				Long sales = Long.parseLong(salesList.get(1));

				//支店定義ファイル内に売上ファイル内の支店コードが存在しなければエラーメッセージを表示して処理を終了
				if (!branchMap.containsKey(branchCode)) {
					System.out.println(fileList[i].getName() + "の支店コードが不正です");
					return;
				}

				//変数を宣言して合計金額を格納
				long salesFee = salesMap.get(branchCode) + sales;

				//売上の合計金額が10桁を超えた場合はエラーメッセージを表示して処理を終了
				if (String.valueOf(salesFee).matches("[0-9]{11,}")) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				//salesMapに支店コードと合計金額を格納
				salesMap.put(branchCode, salesFee);

			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if (br != null) {
					try {
						//BufferReaderの終了
						br.close();
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}

		//outputFileメソッドを使用して出力
		if(outputFile(args[0], "branch.out", branchMap, salesMap) != true) {
			return;
		}

	}

	//出力処理を行うメソッド
	public static boolean outputFile(String path, String fileName, HashMap<String, String> branchMap,
			HashMap<String, Long> salesMap) {

		File outputFile = new File(path, fileName);

		BufferedWriter bw = null;

		try {
			//新たなファイルを作成処理
			outputFile.createNewFile();
			//FileWriterオブジェクトの生成
			FileWriter fw = new FileWriter(outputFile);
			//BufferedWriterオブジェクトの生成
			bw = new BufferedWriter(fw);

			//作成したファイルに合計金額を出力
			for (Map.Entry<String, String> branchInfo : branchMap.entrySet()) {
				bw.write(branchInfo.getKey() + "," + branchInfo.getValue() + ","
						+ salesMap.get(branchInfo.getKey()));
				bw.newLine();
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
		return true;
	}
}
